# Oracle Kerberos

***

To test Kerberos with ODP.NET either build the solution using the source code or download the pre-built zip file.

## Usage
Update the `App.Config` if building with Visual Studio, otherwise modify the `KerberosTest.exe.config` file. 

```xml
<appSettings>
    <add key="DataSource" value="" />
    <add key="UserID" value="/" />
</appSettings>
```

You can add values for the following:

* DataSource
* TnsAdmin
* UserID
* Password
* WalletLocation

The run the application. 
