﻿using System;
using System.Configuration;
using System.IO;
using System.Reflection;
using Oracle.ManagedDataAccess.Client;

namespace KerberosTest
{
    internal class Program
    {
        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.AssemblyResolve += CurrentDomain_AssemblyResolve;

            OracleConnectionStringBuilder connectionStringBuilder = new OracleConnectionStringBuilder();

            foreach (var key in ConfigurationManager.AppSettings.AllKeys)
            {
                switch (key)
                {
                    case "DataSource":
                        connectionStringBuilder.DataSource = ConfigurationManager.AppSettings["DataSource"];
                        break;
                    case "TnsAdmin":
                        connectionStringBuilder.TnsAdmin = ConfigurationManager.AppSettings["TnsAdmin"];
                        break;
                    case "UserID":
                        connectionStringBuilder.UserID = ConfigurationManager.AppSettings["UserID"];
                        break;
                    case "Password":
                        connectionStringBuilder.Password = ConfigurationManager.AppSettings["Password"];
                        break;
                    case "WalletLocation":
                        connectionStringBuilder.WalletLocation = ConfigurationManager.AppSettings["WalletLocation"];
                        break;
                }
            }
            using (var conn = new OracleConnection(connectionStringBuilder.ConnectionString))
            {
                using (OracleCommand oracleCommand = conn.CreateCommand())
                {
                    try
                    {
                        conn.Open();
                        oracleCommand.CommandText = "SELECT 2*4 FROM DUAL";
                        var result = oracleCommand.ExecuteScalar();
                        Console.WriteLine($"Success 2*4 is {result}");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"Error: {ex.Message}");
                        Console.WriteLine($"{ex.StackTrace}");
                    }
                }
            }

            Console.ReadKey();
        }

        static Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            if (args.Name.Contains("Oracle.ManagedDataAccessIOP"))
            {
                return TryAssemblyResolve(sender, args);
            }
            return null;
        }

        private static Assembly TryAssemblyResolve(object sender, ResolveEventArgs e)
        {
            try
            {
                string[] assemblyDetail = e.Name.Split(',');
                string assemblyBasePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                string assemblyPath;
                if (Environment.Is64BitProcess)
                {
                    assemblyPath = Path.Combine(assemblyBasePath, "x64", assemblyDetail[0] + ".dll");
                }
                else
                {
                    assemblyPath = Path.Combine(assemblyBasePath, "x86", assemblyDetail[0] + ".dll");
                }
                return Assembly.UnsafeLoadFrom(assemblyPath); // Allow to run from UNC Path
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Failed resolving assembly {e.Name}: {ex.Message}");
                return null;
            }
        }
    }

}
